<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
      return view('admin.index');
    }
    public function add(Request $reqeust){
      $status = ($reqeust->gasA + $reqeust->gasB)/2;
      $data = new \App\CircleMaps;
      $data->lat = $reqeust->lat;
      $data->long = $reqeust->long;
      $data->gasA = $reqeust->gasA;
      $data->gasB = $reqeust->gasB;
      $data->radius = $reqeust->radius;
      $data->lokasi = $reqeust->lokasi;
      if($status < 51){
        $data->status = "Baik";
        $data->deskripsi = "Tidak memberikan dampak bagi kesehatan manusia atau hewan.";
      }
      if($status > 50 && $status <101){
        $data->status = "Sedang";
        $data->deskripsi = "Tidak berpengaruh pada kesehatan manusia ataupun hewan tetapi berpengaruh pada tumbuhan yang peka.";
      }
      if($status > 100 && $status <200){
        $data->status = "Tidak Sehat";
        $data->deskripsi = "Bersifat merugikan pada manusia ataupun kelompok hewan yang peka atau dapat menimbulkan kerusakan pada tumbuhan ataupun nilai estetika.";
      }
      if($status > 200 && $status <300){
        $data->status = "Sangat Tidak Sehat";
        $data->deskripsi = "kualitas udara berbahaya yang secara umum dapat merugikan kesehatan yang serius pada populasi (misalnya iritasi mata, batuk, dahak dan sakit tenggorokan).";
      }
      if($status > 300){
        $data->status = "Berbahaya";
        $data->deskripsi = "Kualitas udara yang dapat merugikan kesehatan pada sejumlah segmen populasi yang terpapar.";
      }
      $data->save();
      $log= new \App\LogActivity;
      $log->lokasi = $reqeust->lokasi;
      $log->status = 'create';
      $log->save();
      
      $web = \App\Web::find(1);
      $web->loct_indexed = $web->loct_indexed +1;
      $web->save();
      return redirect()->back();
    }

    public function edit(Request $reqeust, $id){
      $status = ($reqeust->gasA + $reqeust->gasB)/2;
      $data = \App\CircleMaps::find($id);
      $data->lat = $reqeust->lat;
      $data->long = $reqeust->long;
      $data->gasA = $reqeust->gasA;
      $data->gasB = $reqeust->gasB;
      $data->radius = $reqeust->radius;
      $data->lokasi = $reqeust->lokasi;
      if($status < 51){
        $data->status = "Baik";
        $data->deskripsi = "Tidak memberikan dampak bagi kesehatan manusia atau hewan.";
      }
      if($status > 50 && $status <101){
        $data->status = "Sedang";
        $data->deskripsi = "Tidak berpengaruh pada kesehatan manusia ataupun hewan tetapi berpengaruh pada tumbuhan yang peka.";
      }
      if($status > 100 && $status <200){
        $data->status = "Tidak Sehat";
        $data->deskripsi = "Bersifat merugikan pada manusia ataupun kelompok hewan yang peka atau dapat menimbulkan kerusakan pada tumbuhan ataupun nilai estetika.";
      }
      if($status > 200 && $status <300){
        $data->status = "Sangat Tidak Sehat";
        $data->deskripsi = "kualitas udara berbahaya yang secara umum dapat merugikan kesehatan yang serius pada populasi (misalnya iritasi mata, batuk, dahak dan sakit tenggorokan).";
      }
      if($status > 300){
        $data->status = "Berbahaya";
        $data->deskripsi = "Kualitas udara yang dapat merugikan kesehatan pada sejumlah segmen populasi yang terpapar.";
      }
      $data->save();
      $log= new \App\LogActivity;
      $log->lokasi = $reqeust->lokasi;
      $log->status = 'update';
      $log->save();
      return redirect()->back();
    }

    public function destroy($id){
      $data = \App\CircleMaps::find($id);
      // dd($data);
      $log= new \App\LogActivity;
      $log->lokasi = $data->lokasi;
      $log->status = 'delete';
      $log->save();
      $data->delete();
      $web = \App\Web::find(1);
      $web->loct_indexed = $web->loct_indexed -1;
      $web->save();
      return redirect()->back();
    }

}
