<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientnController extends Controller
{
  public function index(){
    $web = \App\Web::find(1);
    $web->page_view =  $web->page_view +1;
    $web->save();
    $data['maps'] = \App\CircleMaps::get();
    $data['artikel'] =\App\CircleMaps::orderBy('gasA','Asc')->paginate(6);
    $data['maps2'] = \App\CircleMaps::pluck('lokasi');
    $data['je2'] = json_encode($data['maps2']);
    $data['je'] = json_encode($data['maps']);
    $data['ja'] = json_decode($data['je']);
    return view('features.maps-circle')->with($data);
  }
  public function apiSearch(Request $request){
    $data = \App\CircleMaps::where('lokasi','LIKE','%'.$request->lokasi)->select('lokasi as name')->get();
    return response()->json($data);
  }
  public function search(Request $request){
    $web = \App\Web::find(1);
    $web->page_view =  $web->page_view +1;
    $web->save();
    $data['maps'] = \App\CircleMaps::where('lokasi',$request->lokasi)->get();
    $data['artikel'] =\App\CircleMaps::where('lokasi',$request->lokasi)->get();
    $data['maps2'] = \App\CircleMaps::where('lokasi',$request->lokasi)->get();
    $data['je2'] = json_encode($data['maps2']);
    $data['je'] = json_encode($data['maps']);
    $data['ja'] = json_decode($data['je']);
    // dd($data['maps'] );
    return view('features.maps-circle2')->with($data);
  }
  public function infoLoc($location){
    $web = \App\Web::find(1);
    $web->page_view =  $web->page_view +1;
    $web->save();
    $location = str_replace("-"," ",$location);
    $data['maps'] = \App\CircleMaps::where('lokasi',$location)->get();
    $data['artikel'] =\App\CircleMaps::where('lokasi',$location)->get();
    $data['maps2'] = \App\CircleMaps::where('lokasi',$location)->get();
    $data['je2'] = json_encode($data['maps2']);
    $data['je'] = json_encode($data['maps']);
    $data['ja'] = json_decode($data['je']);
    // dd($data['maps'] );
    return view('features.maps-circle2')->with($data);
  }
  public function MachinePost($id,$gasA,$gasB){
    $status = ($gasA + $gasB)/2;
    $static = \App\CircleMaps::find($id);
    $static->gasA = $gasA;
    $static->gasB = $gasB;
    if($status < 51){
      $static->status = "Baik";
      $static->deskripsi = "Tidak memberikan dampak bagi kesehatan manusia atau hewan.";
    }
    if($status > 50 && $status <101){
      $static->status = "Sedang";
      $static->deskripsi = "Tidak berpengaruh pada kesehatan manusia ataupun hewan tetapi berpengaruh pada tumbuhan yang peka.";
    }
    if($status > 100 && $status <200){
      $static->status = "Tidak Sehat";
      $static->deskripsi = "Bersifat merugikan pada manusia ataupun kelompok hewan yang peka atau dapat menimbulkan kerusakan pada tumbuhan ataupun nilai estetika.";
    }
    if($status > 200 && $status <300){
      $static->status = "Sangat Tidak Sehat";
      $static->deskripsi = "kualitas udara berbahaya yang secara umum dapat merugikan kesehatan yang serius pada populasi (misalnya iritasi mata, batuk, dahak dan sakit tenggorokan).";
    }
    if($status > 300){
      $static->status = "Berbahaya";
      $static->deskripsi = "Kualitas udara yang dapat merugikan kesehatan pada sejumlah segmen populasi yang terpapar.";
    }
    $static->save();
    $log= new \App\LogActivity;
    $log->lokasi = $static->lokasi;
    $log->status = 'update';
    $log->save();

    return response()->json("ok");
  }
  public function MachineGet($id){
    $data = \App\CircleMaps::find($id);
    $machine = array($data->lokasi=>$data);
    return response()->json($machine);
  }
  public function MachineGetAll(){
    $data = \App\CircleMaps::get();
    for($i=0; $i<count($data); $i++){
      $machine[$i] = array($data[$i]->lokasi=>$data[$i]);
    }
    return response()->json($machine);
  }
}
