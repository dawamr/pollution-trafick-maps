"use strict";

var x=new google.maps.LatLng(-6.2098521,106.7629784);
var infoWindow;
var initial = {
  center:x,
  zoom:11,
  mapTypeId: 'terrain'
  };

var map=new google.maps.Map(document.getElementById("map"),initial);

function initialize()
{
var loc0004=new google.maps.LatLng(-6.277768,106.705677);
//penanda titik dumet grogol
var loc0005=new google.maps.LatLng(-6.1690215,106.9005005);
//penanda titik dumet srengseng
var loc0006=new google.maps.LatLng(-6.2098521,106.9029784);
var zona002=[loc0004,loc0005,loc0006,loc0004];
var load_zona002=new google.maps.Polygon({
  path:zona002,
  strokeColor: '#FF0000',
  strokeOpacity: 0.8,
  strokeWeight: 3,
  fillColor: '#FF0000',
  fillOpacity: 0.35
  });

load_zona002.setMap(map);

// Add a listener for the click event.
load_zona002.addListener('click', showArrays);
infoWindow = new google.maps.InfoWindow;
}

/** @this {google.maps.Polygon} */
  function showArrays(event) {
    // Since this polygon has only one path, we can call getPath() to return the
    // MVCArray of LatLngs.
    var vertices = this.getPath();

    var content001 = '<b>Bermuda Triangle polygon</b><br>' +
        'Clicked location: <br>' + event.latLng.lat() + ',' + event.latLng.lng() +
        '<br>';

    // Iterate over the vertices.
    for (var i =0; i < vertices.getLength(); i++) {
      var xy = vertices.getAt(i);
      content001 += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' +
          xy.lng();
    }

    // Replace the info window's content and position.
    infoWindow.setContent(content001);
    infoWindow.setPosition(event.latLng);

    infoWindow.open(map);
  }

google.maps.event.addDomListener(window, 'load', initialize);
