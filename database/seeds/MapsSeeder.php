<?php

use Illuminate\Database\Seeder;

class MapsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $maps =new \App\CircleMaps;
        $maps->lat = "-6.964464";
        $maps->long = "110.427810";
        $maps->gasA = 30;
        $maps->gasB = 40;
        $maps->radius = 650;
        $maps->lokasi = "Stasiun Tawang";
        $maps->kecamatan = "Semarang Utara"; 
        $maps->kota = "Kota Semarang";
        $maps->status = "Baik";
        $maps->deskripsi = "Tidak memberikan dampak bagi kesehatan manusia atau hewan";
        $maps->save();
        $maps =new \App\CircleMaps;
        $maps->lat = "-6.973073";
        $maps->long = "110.414708";
        $maps->gasA = 70;
        $maps->gasB = 60;
        $maps->radius = 650;
        $maps->lokasi = "Stasiun Poncol";
        $maps->kecamatan = "Semarang Utara";
        $maps->kota = "Kota Semarang";
        $maps->status = "Sedang";
        $maps->deskripsi = "Tidak berpengaruh pada kesehatan manusia ataupun hewan tetapi berpengaruh pada tumbuhan yang peka";
        $maps->save();
        $maps =new \App\CircleMaps;
        $maps->lat = "-6.947072";
        $maps->long = "110.423999";
        $maps->gasA = 100;
        $maps->gasB = 110;
        $maps->radius = 650;
        $maps->lokasi = "Pelabuhan Tanjung Mas";
        $maps->kecamatan = "Semarang Utara";
        $maps->kota = "Kota Semarang";
        $maps->status = "Tidak Sehat";
        $maps->deskripsi = "Bersifat merugikan pada manusia ataupun kelompok hewan yang peka atau dapat menimbulkan kerusakan pada tumbuhan ataupun nilai estetika.";
        $maps->save();
        $maps =new \App\CircleMaps;
        $maps->lat = "-6.990236";
        $maps->long = "110.420286";
        $maps->gasA = 300;
        $maps->gasB = 330;
        $maps->radius = 650;
        $maps->lokasi = "SMK N 8 Semarang";
        $maps->kecamatan = "Semarang Selatan";
        $maps->kota = "Kota Semarang";
        $maps->status = "Berbahaya";
        $maps->deskripsi = "Kualitas udara yang dapat merugikan kesehatan pada sejumlah segmen populasi yang terpapar.";
        $maps->save();
        $web = new \App\Web;
        $web->page_view=0;
        $web->loct_indexed=4;
        $web->save();
    }
}
