@extends('layouts.admin')
@section('css')
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<link rel="stylesheet" href="/stisla/assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="/stisla/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/stisla/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
<style media="screen">
  .tx-1{
    font-size: 10px !important;
  }
</style>
@endsection
@section('content')
<?php
  $maps = \App\CircleMaps::orderBy('updated_at','Desc')->get();
  $maps2 = \App\CircleMaps::pluck('lokasi');
  $je2 = json_encode($maps2);
  $je = json_encode($maps);
  $ja = json_decode($je);
  $i=1;
  $log=\App\LogActivity::get();
  // dd($ja[$i]);
 ?>
<div class="card">
  <div class="card-header">
    <h4>MAPS CIRCLE</h4>
  </div>
  <div class="card-body">
    <div id="map" data-height="400"></div>
    <br>
  </div>

</div>

<div class="card">
  <div class="card-header">
    <h4>DATA MAPS</h4>
  </div>
  <div class="card-body">

    <div class="table-responsive">
      <!-- <a href="#addAPI" id="btAdd" class="btn btn-success mb-3"><i class="fas fa-plus"></i> Tambah Data</a> -->
        <form action="/admin/features/add" id="frSubmit" method="post">
          {{csrf_field()}}
          <table id="addAPI" class="table table-responsive mb-5">
            <tr>
              <td>
              <input type="number" name="id" id="inId" value="" hidden>
                <input type="text" placeholder="Location" name="lokasi" id="inLokasi" class="form-control input-sm">
              </td>
              <td>
                <input type="text" placeholder="Latitude" name="lat" id="inLat" class="form-control input-sm">
              </td>
              <td>
                <input type="text" placeholder="Longtitude" name="long" id="inLong" class="form-control input-sm">
              </td>
              <td>
                <input type="text" placeholder="Sub-district" name="kecamatan" id="inKecamatan" class="form-control input-sm">
              </td>
            </tr>
            <tr>
              <td>
                <input type="text" placeholder="City" name="kota" id="inKota" class="form-control input-sm">
              </td>
              <td>
                <input type="text" placeholder="GasA" name="gasA" id="inGasA" class="form-control input-sm">
              </td>
              <td>
                <input type="text" placeholder="GasB" name="gasB" id="inGasB" class="form-control input-sm">
              </td>
              <td>
                <input type="text" placeholder="Radius (m)"  name="radius" id="inRadius"class="form-control input-sm">
              </td>
            </tr>
            <tr>
            <td>
              <button type="submit" id="btSubmit" class="btn btn-primary btn-block"><i class="fas fa-plus"></i>Add Data</button>              
            </td>
            <td>
            <button type="button" onClick="btnCancel()" id="btCancel" class="btn btn-danger" style="visibility:hidden"><i class="fas fa-times-circle text-white"></i></button>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
          </table>
          <script>
            function btnCancel() {
              document.getElementById("frSubmit").action = "/admin/features/add";
              document.getElementById("inId").value = "";
              document.getElementById("inLokasi").value = "";
              document.getElementById("inLat").value = "";
              document.getElementById("inLong").value = "";
              document.getElementById("inGasA").value = "";
              document.getElementById("inGasB").value = "";
              document.getElementById("inRadius").value = "";
              document.getElementById("inKecamatan").value = "";
              document.getElementById("inKota").value = "";
              document.getElementById("btSubmit").innerHTML = "Add Data";
              document.getElementById("btCancel").style.visibility = "hidden";
            }
          </script>
        </form>
        <table class="table table-striped" id="table-1">
          <thead>
            <tr>
              <th>api</th>
              <th class="text-center">Location</th>
              <th class="text-center">Latitude</th>
              <th class="text-center">Longtitude</th>
              <th class="text-center">Sub-district</th>
              <th class="text-center">City</th>
              <th class="text-center">Description</th>
              <th class="text-center">Opsi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($maps as $data)
            <tr>
              <td>{{($data->gasA+$data->gasB)/2}}</td>
              <td>{{$data->lokasi}}</td>
              <td>{{$data->lat}}</td>
              <td>{{$data->long}}</td>
              <td>{{$data->kecamatan}}</td>
              <td>{{$data->kota}}</td>
              <td>
                <p>
                <b>API : {{($data->gasA+$data->gasB)/2}} || {{$data->status}}</b> <hr>
                <details>
                Machine A : {{$data->gasA}}
                Machine B : {{$data->gasB}}
                Radius : @if(strlen($data->radius) >=4) {{$data->radius/1000}} KM @endif @if(strlen($data->radius) < 4) {{$data->radius}} M @endif 
                <br>
                {{$data->deskripsi}}
                </details>
                </p>
              </td>
              <td> 
              <form action="/admin/{{$data->id}}" method="post">
                {{csrf_field()}}
                <button type="submit" class="btn btn-danger tx-1"><i class="fas fa-trash-alt"></i></button>
                <a href="#addAPI" onclick="btnEdit{{$i}}()" class="btn btn-primary text-white tx-1"><i class="fas fa-pencil-ruler"></i></a>
              </form>
              <script>
                function btnEdit{{$i}}() {
                  document.getElementById("frSubmit").action = "/admin/features/{{$data->id}}/edit";
                  document.getElementById("inId").value = "{{$data->id}}";
                  document.getElementById("inLokasi").value = "{{$data->lokasi}}";
                  document.getElementById("inLat").value = "{{$data->lat}}";
                  document.getElementById("inLong").value = "{{$data->long}}";
                  document.getElementById("inGasA").value = "{{$data->gasA}}";
                  document.getElementById("inGasB").value = "{{$data->gasB}}";
                  document.getElementById("inKecamatan").value = "{{$data->kecamatan}}";
                  document.getElementById("inKota").value = "{{$data->kota}}";
                  document.getElementById("inRadius").value = "{{$data->radius}}";
                  document.getElementById("btSubmit").innerHTML = "Save";
                  document.getElementById("btCancel").style.visibility = "visible";
                }
              </script>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
  </div>
  <div class="card-footer bg-whitesmoke"></div>
  <div class="card-header">
    <h4>Log Activity</h4>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-striped" id="table-2">
        <thead>
          <tr>
            <td>#</td>
            <td>Location</td>
            <td>Action</td>
            <td>Action Time</td>
          </tr>
        </thead>
        <tbody>
          @foreach($log as $data)
              <tr>
                <td>{{$i++}}</td>
                <td>{{strtoupper(strtolower($data->lokasi))}}</td>
                <td><b>{{strtoupper(strtolower($data->status))}}</b></td>
                <td>{{$data->updated_at->diffForHumans()}}</td>              
              </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer bg-whitesmoke"></div>
</div>

@endsection
@section('js')

<script src="/stisla/assets/modules/jquery-ui/jquery-ui.min.js"></script>
<script async defer src="http://maps.google.com/maps/api/js?key=AIzaSyB55Np3_WsZwUQ9NS7DP-HnneleZLYZDNw&language=id&callback=initMap"></script>
<script>
$("#addApi").hide();
$('#btAdd').click(function(){
    $("#addApi").show();
    $("#btAdd").hide();
});
</script>
<script>
    var zoom = 12.5;
    var center = {lat:-6.983074, lng: 110.422002};
</script>
@include('features.script')

 <script src="/stisla/assets/modules/datatables/datatables.min.js"></script>
 <script src="/stisla/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
 <script src="/stisla/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
 <script src="/stisla/assets/modules/jquery-ui/jquery-ui.min.js"></script>
 <script>
   $("#table-1").dataTable({
   "columnDefs": [
       { "sortable": true, "targets": [2,3] }
   ]
   });
   $("#table-2").dataTable({
   "columnDefs": [
       { "sortable": true, "targets": [2,3] }
   ]
   });
   var table2 = $('#table-2').DataTable();
    table2.column(0).visible(false);
    var table1 = $('#table-1').DataTable();
    table1.column(0).visible(false);
 </script>
@endsection
