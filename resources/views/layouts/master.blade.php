<?php
$web =\App\Web::get()->first();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>E-POLLUTION &mdash; {{date('Y')}}</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="/stisla/assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/stisla/assets/modules/fontawesome/css/all.min.css">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="/stisla/assets/modules/jquery-selectric/selectric.css">
  <link href="/css/select2.min.css" rel="stylesheet" />
  <!-- Template CSS -->
  <link rel="stylesheet" href="/stisla/assets/css/style.css">
  <link rel="stylesheet" href="/stisla/assets/css/components.css">
  @yield('css')
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script src="/stisla/assets/modules/jquery.min.js"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body class="layout-3">
  <div id="app">
      <nav class="navbar navbar-default navbar-expand-lg bg-primary main-navbar">
        <a href="index.html" class="navbar-brand sidebar-gone-hide">MAPS POLLUTION</a>
        <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
        <div class="nav-collapse">
          <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
            <i class="fas fa-ellipsis-v"></i>
          </a>
          <ul class="navbar-nav">
            <li class="nav-item"><a href="#maps" class="nav-link">Maps Pollution</a></li>
            <li class="nav-item"><a href="#index" class="nav-link">Index Pollution</a></li>
            <li class="nav-item"><a href="#area" class="nav-link">Pullution Area</a></li>
          </ul>
        </div>
        <div class="form-inline ml-auto">
          <ul class="navbar-nav">
          <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
          <form action="/search" method="get">
            <input class="typeahead form-control" name="lokasi" id="mySearch" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" onclick="oke()" id="btnSearch" type="submit"><i class="fas fa-search"></i></button>
            <script>
              
              var inputKey = 0;
              const inputValue = document.getElementById("mySearch");

              function oke(){
                inputKey +=1;
                const key  = inputKey;
                const Nilai = inputValue.value;
                console.log(key);
                console.log(Nilai);
                if(key && Nilai){
                  localStorage.setItem(key, Nilai);
                  // location.reload();
                }
              }
            </script>
          </form>
            <div class="search-backdrop"></div>
            <div class="search-result">
              <div class="search-header">
                Histories
              </div>
              <div class="search-item">
                <a href="#">No History</a>
                <a href="#" class="search-close"><i class="fas fa-trash"></i></a>
              </div>
              <div class="search-header">
                Result
              </div>
              <div class="search-item">
                <a href="#">
                  <!-- <img class="mr-3 rounded" width="30" src="#" alt="product"> -->
                  No Result
                </a>
              </div>
            </div>
          </div>
        </div>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="far fa-bell"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
              <div class="dropdown-header">Notifications
              </div>
              <?php $datas = \App\CircleMaps::orderBy('updated_at','desc')->limit(7)->get() ?>
              
              <div class="dropdown-list-content dropdown-list-icons">
              @foreach($datas as $data)
              <?php $value =   ($data->gasA +$data->gasB)/2 ?> 
                <a href="#" class="dropdown-item dropdown-item-unread">
                      @if($value>0 && $value < 51 )
                      <div class="dropdown-item-icon text-white" style="background-image : linear-gradient(to right, #11c614, #00ff2e)">
                        <i class="fas fa-map-marker-alt"></i>
                      </div>
                      @endif
                      @if($value>50 && $value< 101)
                      <div class="dropdown-item-icon text-white" style="background-image : linear-gradient(to right, #f7e418, #e8e400)">
                        <i class="fas fa-map-marker-alt"></i>
                      </div>
                      @endif
                      @if($value>100 && $value< 200)
                      <div class="dropdown-item-icon text-white" style="background-image : linear-gradient(to right, #ff9400, #ffc300)">
                        <i class="fas fa-map-marker-alt"></i>
                      </div>
                      @endif
                      @if($value>200 && $value< 301)
                      <div class="dropdown-item-icon text-white" style="background-image : linear-gradient(to right, #ff0c0c, #ff0000)">
                        <i class="fas fa-map-marker-alt"></i>
                      </div>
                      @endif
                      @if($value>300)
                      <div class="dropdown-item-icon text-white" style="background-image : linear-gradient(to right,  #700000, #c10303)">
                        <i class="fas fa-map-marker-alt"></i>
                      </div>
                      @endif
                  <div class="dropdown-item-desc">
                    {{$data->lokasi}} <br >
                    API : <b>{{$value}}</b> || STATUS : {{$data->status}}
                    <div class="time text-primary">{{$data->updated_at->diffForHumans()}}</div>
                  </div>
                </a>
              @endforeach
              </div>
              
              <div class="dropdown-footer text-center">
                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
              </div>
            </div>
          </li>
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="/stisla/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, Wellcome</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title">Page Visited :{{$web->page_view}}</div>
              <div class="dropdown-title">Place Indexed :{{$web->loct_indexed}}</div>
            </div>
          </li>
        </ul>
      </nav>
    <div class="card-body">
    <section class="section"> 
      <div class="container mt-5">      
        <div class="row">
          <div class="col-md-12">
            <div class="login-brand">
            
              <h2 class="text-center">FOG COMPUTING</h2><h5>UNTUK MONITORING SYSTEM PEMERINTAH DAERAH <br>RAPAT POLUSI 
              UDARA KARBON MONOKSIDA MENGGUNAKAN METODE <br> ORDINARY KRIGING</h5>
            </div>

            @yield('content')

            <div class="simple-footer text-center">
              Copyright &copy; Air Pollutan
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <!-- General JS Scripts -->
  <script src="/stisla/assets/modules/jquery.min.js"></script>
  <script src="/stisla/assets/modules/popper.js"></script>
  <script src="/stisla/assets/modules/tooltip.js"></script>
  <script src="/stisla/assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="/stisla/assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="/stisla/assets/modules/moment.min.js"></script>
  <script src="/stisla/assets/js/stisla.js"></script>
  <script>
    function validate(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
    // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
    }
  </script>
  <!-- JS Libraies -->
  <script src="/stisla/assets/modules/jquery-pwstrength/jquery.pwstrength.min.js"></script>
  <script src="/stisla/assets/modules/jquery-selectric/jquery.selectric.min.js"></script>
  <script src="/js/select2.min.js"></script>
  <script src="/js/typeahead.min.js"></script>
  <script>
  var path = "/api/search";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });
  </script>
  
  <!-- Page Specific JS File -->
  <script src="/stisla/assets/js/page/auth-register.js"></script>
  @yield('js')
  <!-- Template JS File -->
  <script src="/stisla/assets/js/scripts.js"></script>
  <script src="/stisla/assets/js/custom.js"></script>
</body>
</html>
