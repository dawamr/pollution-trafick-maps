<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>E-POLLUTION &mdash; {{date('Y')}}</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="/stisla/assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/stisla/assets/modules/fontawesome/css/all.min.css">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="/stisla/assets/css/style.css">
  <link rel="stylesheet" href="/stisla/assets/css/components.css">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
@yield('css')
<!-- /END GA -->
</head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="/stisla/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">{{\Auth::user()->name}}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title">Register IN <br> {{Auth::user()->created_at}}</div>
              <a href="features-profile.html" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <a href="features-activities.html" class="dropdown-item has-icon">
                <i class="fas fa-bolt"></i> Activities
              </a>
              <a href="features-settings.html" class="dropdown-item has-icon">
                <i class="fas fa-cog"></i> Settings
              </a>
              <div class="dropdown-divider"></div>
              <a href="/logout" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">AIR POLLUTAN</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">API</a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">MENU</li>
            <li class="dropdown">

              <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Features</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="#map">CIRCLE MAPS</a></li>
                <li><a class="nav-link" href="#addAPI">NEW POI</a></li>
                <li><a class="nav-link" href="#table-1">POINT OF INTEREST (POI)</a></li>
                <li><a class="nav-link" href="#table-2">LOG ACTIVITY</a></li>
              </ul>
            </li>
            <li><a class="nav-link" href="/logout"><i class="fas fa-sign-out" aria-hidden="true"></i> <span>Sign Out</span></a></li>


          </ul>

          <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
              <i class="fas fa-coffee"></i> {{strtoupper('D o n a t i o n')}}
            </a>
          </div>        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Default Layout</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
              <div class="breadcrumb-item"><a href="#">Layout</a></div>
              <div class="breadcrumb-item">Default Layout</div>
            </div>
          </div>

          <div class="section-body">
            @yield('content')


          </div>
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
        Copyright &copy; Air Pollutan
        </div>
        <div class="footer-right">

        </div>
      </footer>
    </div>
  </div>


  <!-- General JS Scripts -->
  <script src="/stisla/assets/modules/jquery.min.js"></script>
  <script src="/stisla/assets/modules/popper.js"></script>
  <script src="/stisla/assets/modules/tooltip.js"></script>
  <script src="/stisla/assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="/stisla/assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="/stisla/assets/modules/moment.min.js"></script>
  <script src="/stisla/assets/js/stisla.js"></script>

  <!-- JS Libraies -->
@yield('js')
  <!-- Page Specific JS File -->

  <!-- Template JS File -->
  <script src="/stisla/assets/js/scripts.js"></script>
  <script src="/stisla/assets/js/custom.js"></script>
</body>
</html>
