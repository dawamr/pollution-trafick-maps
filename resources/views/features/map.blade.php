@extends('layouts.master')
@section('css')
  <!-- CSS Libraries -->

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
@endsection
@section('content')
<?php
  $zonas = \App\Zona::get();
 ?>
    <div class="card card-primary">
        <div class="card-header"><h4>Pilih Lomba</h4></div>

        <div class="card-body">

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                    <div id="map" data-height="400"></div>
                  </div>
                </div>
              </div>
            </div>

        </div>
    </div>
    <script src="/stisla/assets/modules/jquery-ui/jquery-ui.min.js"></script>

     <!-- JS Libraies -->
   <script src="http://maps.google.com/maps/api/js?key=AIzaSyB55Np3_WsZwUQ9NS7DP-HnneleZLYZDNw&amp;sensor=true"></script>
   <script src="/stisla/assets/modules/gmaps.js"></script>
   <script type="text/javascript">
   "use strict";

   var x=new google.maps.LatLng(-7.0061977,110.4151001);
   var infoWindow;
   var initial = {
     center:x,
     zoom:12,
     mapTypeId: 'terrain'
   };

   var map=new google.maps.Map(document.getElementById("map"),initial);
   </script>
    @foreach($zonas as $zona)
    <?php
    $locxads = \App\Maps::where('zona_id', $zona->id)->get();
    ?>
    @for($i=0; $i< count($locads);$i++)

    <!-- Page Specific JS File -->
    <script type="text/javascript">
      function initialize()
      {
      var loc1 =new google.maps.LatLng({{$locads[0]->lat}},{{$locads[0]->long}});
      //penanda titik dumet grogol
      var loc2=new google.maps.LatLng({{$locads[1]->lat}},{{$locads[1]->long}});
      //penanda titik dumet srengseng
      var loc3=new google.maps.LatLng({{$locads[2]->lat}},{{$locads[2]->long}});
      var zona{{$zona->id}}=[loc1,loc2,loc3,loc1];
      var load_zona{{$zona->id}}=new google.maps.Polygon({
        path:zona{{$zona->id}},
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#FF0000',
        fillOpacity: 0.35
        });

      load_zona{{$zona->id}}.setMap(map);

      // Add a listener for the click event.
      load_zona{{$zona->id}}.addListener('click', showArrays);
      infoWindow = new google.maps.InfoWindow;
      }

      /** @this {google.maps.Polygon} */
        function showArrays(event) {
          // Since this polygon has only one path, we can call getPath() to return the
          // MVCArray of LatLngs.
          var vertices = this.getPath();

          var content001 = '<b>Bermuda Triangle polygon</b><br>' +
              'Clicked location: <br>' + event.latLng.lat() + ',' + event.latLng.lng() +
              '<br>';

          // Iterate over the vertices.
          for (var i =0; i < vertices.getLength(); i++) {
            var xy = vertices.getAt(i);
            content001 += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' +
                xy.lng();
          }

          // Replace the info window's content and position.
          infoWindow.setContent(content001);
          infoWindow.setPosition(event.latLng);

          infoWindow.open(map);
        }

      google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    @endfor
    @endforeach
@endsection
@section('js')
 <!-- JS Libraies -->



<!-- <script src="/stisla/assets/js/page/gmaps-multiple-marker.js"></script> -->
@endsection
