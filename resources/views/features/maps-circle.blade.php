@extends('layouts.master')
@section('css')
  <!-- CSS Libraries -->
  <style>
   .no-gutter > [class*='col-'] {
        padding-right:0 !important;
        padding-left:0 !important;
    }
    .box{
      -webkit-overflow-scrolling: touch;
      overflow-x: scroll;
      overflow-y: hidden;
      white-space: nowrap;
    }
    .box-1{
      display:inline-block;
    }
    .hello{
      white-space: normal;
      word-wrap: break-word;
    }
  </style>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
  <link rel="stylesheet" href="/stisla/assets/modules/datatables/datatables.min.css">
  <link rel="stylesheet" href="/stisla/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="/stisla/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
@endsection
@section('content')
<?php
  $i=1;
 ?>
    <div class="card card-danger" id="maps">
    <div class="card-header"><h4>MAPS AIR POLLUTION INDEX (API)</h4></div>
        <div class="card-body">
            <div class="card">
              <div class="card-body bg-dark">
                <div id="map" data-height="400"></div>
              </div>
            </div>
        </div>
    </div>

    @include('features.api')

    <div class="card card-danger" id="area">
      <div class="card-header"><h4>SEMARANG AREA</h4></div>
        @php $no = 1; @endphp
      <div class="card-body bg-secondary">
        <div class="row"> 
          @foreach($artikel as $data)
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
              <div class="card">
                <div class="card-header">
                  <h4><a href="/info/{{str_replace(' ','-',$data->lokasi)}}">{{$data->lokasi}}</a></h4>
                  <div class="card-header-action">
                    <a data-collapse="#show-hide{{$no}}" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                  </div>
                </div>
                <div class="collapse show" id="show-hide{{$no}}">
                  <div class="card-body">
                    <?php
                        $value= ($data->gasA + $data->gasB)/2;
                        
                        $no = $no+1
                    ?>
                    <table class="table table-bordered" style="word-wrap: break-word;" width="100%">
                      <tr class="bg-dark text-white">
                        <th>API</th>
                        <th>PARAMETER</th>
                      </tr>
                      <tr>
                        <td><strong>{{$value}}</strong></td>
                        <td>CO ( <i>carbon monoxide</i> )</td>
                      </tr>
                      <tr>
                        
                          @if($value>0 && $value < 51 )
                          <td colspan="2" class="text-center text-white" style="background-image : linear-gradient(to right, #11c614, #00ff2e)">
                            {{strtoupper($data->status)}}
                          </td>
                          @endif
                          @if($value>50 && $value< 101)
                          <td colspan="2"  class="text-center text-white" style="background-image : linear-gradient(to right, #f7e418, #e8e400)">
                          {{strtoupper($data->status)}}
                          </td>
                          @endif
                          @if($value>100 && $value< 200)
                          <td colspan="2"  class="text-center text-white" style="background-image : linear-gradient(to right, #ff9400, #ffc300)">
                          {{strtoupper($data->status)}}
                          </td>
                          @endif
                          @if($value>200 && $value< 301)
                          <td colspan="2"  class="text-center text-white" style="background-image : linear-gradient(to right, #ff0c0c, #fc2828)">
                          {{strtoupper($data->status)}}
                          </td>
                          @endif
                          @if($value>300)
                          <td colspan="2"  class="text-center text-white" style="background-image : linear-gradient(to right, #700000, #700000)">
                          {{strtoupper($data->status)}}
                          </td>
                          @endif
                          
                        </td>
                      </tr>
                    </table>
                  </div>
                  <div class="card-footer">
                    {{$data->updated_at}}
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>
       
      </div>
      <div class="card-footer">
      <b>{{$artikel->links()}}</b>
      </div>
    </div>
    
    @include('features.kecamatan')
    <script src="/stisla/assets/modules/jquery-ui/jquery-ui.min.js"></script>
    
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&callback=initialize"></script> -->
    <script async defer src="http://maps.google.com/maps/api/js?key=AIzaSyB55Np3_WsZwUQ9NS7DP-HnneleZLYZDNw&callback=initMap&language=id"></script>
    <script>
    var zoom = 12.5;
    var center = {lat:-6.983074, lng: 110.422002};
    </script>
    @include('features.script')
     <!-- JS Libraies -->



@endsection
@section('js')
 <!-- JS Libraies -->

 <script src="/stisla/assets/modules/datatables/datatables.min.js"></script>
 <script src="/stisla/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
 <script src="/stisla/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
 <script src="/stisla/assets/modules/jquery-ui/jquery-ui.min.js"></script>
 <script>
   $("#table-1").dataTable({
   "columnDefs": [
       { "sortable": true, "targets": [2,3] }
   ]
   });
 </script>

<!-- <script src="/stisla/assets/js/page/gmaps-multiple-marker.js"></script> -->
@endsection
