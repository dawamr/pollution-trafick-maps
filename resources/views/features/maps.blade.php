<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Using Closures in Event Listeners</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <?php
      $maps = \App\CircleMaps::get();
      $maps2 = \App\CircleMaps::pluck('info');
      $je2 = json_encode($maps2);
      $je = json_encode($maps);
      $ja = json_decode($je);
      // dd($ja[1]->lat);
      $i=1;

     ?>
    <div id="map"></div>
    <script>
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: {lat: -7.0061977, lng: 110.4151001},
        });


        // Add 5 markers to map at random locations.
        // For each of these markers, give them a title with their index, and when
        // they are clicked they should open an infowindow with text from a secret
        // message.
        var secretMessages = <?php echo $je2; ?>;

        <?php for($i=0;$i< count($maps2);$i++){ ?>
          var marker = new google.maps.Marker({
            position: {
              lat: <?php echo $ja[$i]->lat ?>,
              lng: <?php echo $ja[$i]->long ?>,
            },
            map: map
          });
           attachSecretMessage(marker, secretMessages[{{$i}}]);
        <?php }?>

      }
      // Attaches an info window to a marker with the provided message. When the
      // marker is clicked, the info window will open with the secret message.
      function attachSecretMessage(marker, secretMessage) {
        var infowindow = new google.maps.InfoWindow({
          content: secretMessage
        });

        marker.addListener('click', function() {
          infowindow.open(marker.get('map'), marker);
        });
      }
    </script>
  <script async defer src="http://maps.google.com/maps/api/js?key=AIzaSyB55Np3_WsZwUQ9NS7DP-HnneleZLYZDNw&callback=initMap&language=id"></script>
  </body>
</html>
