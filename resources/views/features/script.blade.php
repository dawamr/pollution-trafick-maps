<script>
    // This example creates circles on the map, representing populations in North
    // America.

    // First, create an object containing LatLng and population for each city.
    var info = [];
    var citymap = {
    <?php for($i=0;$i<count($ja);$i++) {?>
        kota{{$i}}: {
        id:{{$ja[$i]->id}},
        center: {lat: {{$ja[$i]->lat}}, lng: {{$ja[$i]->long}}},
        // population: 100,
        gasA:{{$ja[$i]->gasA}},
        gasB:{{$ja[$i]->gasB}},
        getRadius:{{$ja[$i]->radius}},
    },
    <?php }; ?>
    };

    function initMap() {
    // Create the map.
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoom,
        center: center,
        mapTypeId: 'satellite'
    });

    // Construct the circle for each value in citymap.
    // Note: We scale the area of the circle based on the population.
    var status,code,ket;
    var i =0;
    var no =[];
    var status = [];
    for (var city in citymap) {
        // console.log(city);
        // Add the circle for this city to the map.

        code = (citymap[city].gasA +citymap[city].gasB)/2;


        if( code > 0 && code < 51){
        status[i] = "Baik";
        var cityCircle = new google.maps.Circle({
            strokeColor: '#00ff2e',
            strokeOpacity: 0.9,
            strokeWeight: 2,
            fillColor: '#00ff2e',
            fillOpacity: 0.7,
            map: map,
            center: citymap[city].center,
            radius: citymap[city].getRadius,
             
        });
        }

        if( code > 50 && code < 101){
        status[i] = "Sedang";
        var cityCircle = new google.maps.Circle({
            strokeColor: '#e8e400',
            strokeOpacity: 0.9,
            strokeWeight: 2,
            fillColor: '#e8e400',
            fillOpacity: 0.7,
            map: map,
            center: citymap[city].center,
            radius: citymap[city].getRadius,
        });
        }

        if( code > 100 && code < 200){
            status[i] = "Tidak Sehat";
        var cityCircle = new google.maps.Circle({
            strokeColor: '#ffc300',
            strokeOpacity: 0.9,
            strokeWeight: 2,
            fillColor: '#ffc300',
            fillOpacity: 0.7,
            map: map,
            center: citymap[city].center,
            radius: citymap[city].getRadius,
        });
        }

        if( code > 200 && code < 301){
            status[i] = "Tidak Sehat";
        var cityCircle = new google.maps.Circle({
            strokeColor: '#ff0c0c',
            strokeOpacity: 0.9,
            strokeWeight: 2,
            fillColor: '#fc2828',
            fillOpacity: 0.7,
            map: map,
            center: citymap[city].center,
            radius: citymap[city].getRadius,
        });
        }

        if( code > 300){
        var cityCircle = new google.maps.Circle({
            strokeColor: '#700000',
            strokeOpacity: 0.9,
            strokeWeight: 2,
            fillColor: '#700000',
            fillOpacity: 0.7,
            map: map,
            center: citymap[city].center,
            radius: citymap[city].getRadius,
        });
        }
        no = status[i];
        var secretMessages = <?php echo $je2; ?>;
        <?php for($i=0;$i< count($maps2);$i++){ ?>
        console.log()
        var content{{$i}} = '<b style="color:black; font-size:13px" >Lokasi : {{$ja[$i]->lokasi}}</b><hr>Latitude : {{$ja[$i]->lat}}<br>Longtitude : {{$ja[$i]->long}} <br>Kadar GAS A : {{$ja[$i]->gasA}} <br>Kadar GAS B : {{$ja[$i]->gasB}} <br>Tingkat Polusi : {{($ja[$i]->gasA+$ja[$i]->gasB)/2}} <br>Status : {{$ja[$i]->status}} <br>Deskrisi : {{$ja[$i]->deskripsi}} <br><br> <a href="/info/{{str_replace(' ','-',$ja[$i]->lokasi)}}">View detail location.</a> <hr> Updated at {{$ja[$i]->updated_at}} '
        var marker = new google.maps.Marker({
            position: {
            lat: <?php echo $ja[$i]->lat ?>,
            lng: <?php echo $ja[$i]->long ?>,
            },
            map: map
        });
            attachSecretMessage(marker, content{{$i}});
        <?php }?>
        i++;
    }



    }
    // Attaches an info window to a marker with the provided message. When the
    // marker is clicked, the info window will open with the secret message.
    function attachSecretMessage(marker, secretMessage) {
    var infowindow = new google.maps.InfoWindow({
        content: secretMessage
    });

    marker.addListener('click', function() {
        infowindow.open(marker.get('map'), marker);
    });

    }

</script>