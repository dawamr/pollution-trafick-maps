@extends('layouts.master')
@section('css')
  <!-- CSS Libraries -->
  <style>
   .no-gutter > [class*='col-'] {
        padding-right:0 !important;
        padding-left:0 !important;
    }
    .box{
      -webkit-overflow-scrolling: touch;
      overflow-x: scroll;
      overflow-y: hidden;
      white-space: nowrap;
    }
    .box-1{
      display:inline-block;
    }
    .hello{
      white-space: normal;
      word-wrap: break-word;
    }
  </style>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
  <link rel="stylesheet" href="/stisla/assets/modules/datatables/datatables.min.css">
  <link rel="stylesheet" href="/stisla/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="/stisla/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
@endsection
@section('content')
<?php
  $i=1;
 ?>
    <div class="card card-danger" id="maps">
    <div class="card-header"><a href="/" class="btn btn-icon btn-secondary"><i class="fas fa-arrow-left"></i> Kembali</a></div>
    <div class="card-header"><h4>MAPS AIR POLLUTION INDEX (API)</h4></div>
        <div class="card-body">
            <div class="card">
                <div class="row">
                    <div class="col-md-8">
                            <div class="card-body bg-dark">
                                <div id="map" data-height="400"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-responsive table-striped">
                                <tr>
                                    <td>Location </td>
                                    <td> : </td>
                                    <td>{{$maps2[0]->lokasi}}</td>
                                </tr>
                                <tr>
                                    <td>Latitude  </td>
                                    <td> : </td>
                                    <td>{{$maps2[0]->lat}}</td>
                                </tr>
                                <tr>
                                    <td>Longtitude  </td>
                                    <td> : </td>
                                    <td>{{$maps2[0]->long}}</td>
                                </tr>
                                <tr>
                                    <td>API </td>
                                    <td> : </td>
                                    <td>{{($maps2[0]->gasA+$maps2[0]->gasB)/2}}</td>
                                </tr>
                                <tr>
                                    <td>Radius </td>
                                    <td> : </td>
                                    <td>@if(strlen($maps2[0]->radius) >=4) {{$maps2[0]->radius/1000}} KM @endif @if(strlen($maps2[0]->radius) < 4) {{$maps2[0]->radius}} M @endif </td>
                                </tr>
                                <tr>
                                    <td>Status </td>
                                    <td> : </td>
                                    <td>{{$maps2[0]->status}}</td>
                                </tr>
                                <tr>
                                    <td>Deskripsi </td>
                                    <td> : </td>
                                    <td>{{$maps2[0]->deskripsi}}</td>
                                </tr>
                            </table>
                        </div>
                </div>    
            </div>
        </div>
    </div>
    @include('features.api')

    
    <script src="/stisla/assets/modules/jquery-ui/jquery-ui.min.js"></script>
    
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&callback=initialize"></script> -->
    <script async defer src="http://maps.google.com/maps/api/js?key=AIzaSyB55Np3_WsZwUQ9NS7DP-HnneleZLYZDNw&callback=initMap"></script>
    <script>
    var zoom = 16.3;
    var center = {lat:{{$maps2[0]->lat}}, lng: {{$maps2[0]->long}}};
    </script>
    @include('features.script')
     <!-- JS Libraies -->



@endsection
@section('js')
 <!-- JS Libraies -->

 <script src="/stisla/assets/modules/datatables/datatables.min.js"></script>
 <script src="/stisla/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
 <script src="/stisla/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
 <script src="/stisla/assets/modules/jquery-ui/jquery-ui.min.js"></script>
 <script>
   $("#table-1").dataTable({
   "columnDefs": [
       { "sortable": true, "targets": [2,3] }
   ]
   });
 </script>

<!-- <script src="/stisla/assets/js/page/gmaps-multiple-marker.js"></script> -->
@endsection
