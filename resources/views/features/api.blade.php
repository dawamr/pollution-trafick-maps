<div class="card card-danger" id="index">
    <div class="card-header"><h4>Air Pollution Index</h4></div>
    <div class="card-body bg-secondary scrolling-wrapper no-gutter box">
    <div class="col-md-3 box-1">
        <div class="card">
        <div class="card-header">
            <h4>API : 0-50</h4>
            <div class="card-header-action">
            <a data-collapse="#data-1" class="btn btn-icon btn-info" href="#"><i class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-1">
            <div class="card-body" data-toggle="collapse" data-target="#data-1" aria-expanded="false" aria-controls="collapseExample"  style="cursor: pointr;background-image : linear-gradient(to right, #11c614, #00ff2e)">
            <h5 class="text-center text-white text-white"> GOOD</h5>
            <div class="collapse" id="data-1">
                <hr>
                <div class="hello text-white">
                Tidak memberikan dampak bagi kesehatan manusia atau hewan.
                </div>
            </div>
            </div>
            
        </div>
        </div>
    </div>
    <div class="col-md-3 box-1">
        <div class="card">
        <div class="card-header">
            <h4>API : 51-100</h4>
            <div class="card-header-action">
            <a data-collapse="#data-2" class="btn btn-icon btn-info" href="#"><i class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-2">
            <div class="card-body" data-toggle="collapse" data-target="#data-2" aria-expanded="false" aria-controls="collapseExample" style="cursor: pointer;background-image : linear-gradient(to right, #f7e418, #e8e400)">
            <h5 class="text-center text-white"> MODERATE</h5>
            <div class="collapse" id="data-2">
                <hr>
                <div class="hello text-white" s>
                Tidak berpengaruh pada kesehatan manusia ataupun hewan tetapi berpengaruh pada tumbuhan yang peka.
                </div>
            </div>
            </div>
        </div>
        </div>
        </div>
    <div class="col-md-3 box-1">
        <div class="card">
        <div class="card-header">
            <h4>API : 101-199</h4>
            <div class="card-header-action">
            <a data-collapse="#data-3" class="btn btn-icon btn-info" href="#"><i class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-3">
            <div class="card-body" data-toggle="collapse" data-target="#data-3" aria-expanded="false" aria-controls="collapseExample" style="cursor:pointer;background-image : linear-gradient(to right, #ff9400, #ffc300)">
            <h5 class="text-center text-white"> UNHEALTHY</h5>
            <div class="collapse" id="data-3">
                <hr>
                <div class="hello text-white">
                Bersifat merugikan pada manusia ataupun kelompok hewan yang peka atau dapat menimbulkan kerusakan pada tumbuhan ataupun nilai estetika.
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    <div class="col-md-3 box-1">
        <div class="card">
        <div class="card-header">
            <h4>API : 201-300</h4>
            <div class="card-header-action">
            <a data-collapse="#data-4" class="btn btn-icon btn-info" href="#"><i class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-4">
            <div class="card-body" data-toggle="collapse" data-target="#data-4" aria-expanded="false" aria-controls="collapseExample" style="cursor:pointer;background-image : linear-gradient(to right, #ff0c0c, #fc2828)">
            <h5 class="text-center text-white">VERY UNHEALTHY</h5>
            <div class="collapse" id="data-4">
                <hr>
                <div class="hello text-white">
                Kualitas udara yang dapat merugikan kesehatan pada sejumlah segmen populasi yang terpapar.
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    <div class="col-md-3 box-1">
        <div class="card">
        <div class="card-header">
            <h4>API : >300</h4>
            <div class="card-header-action">
            <a data-collapse="#data-5" class="btn btn-icon btn-info" href="#"><i class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-5">
            <div class="card-body" data-toggle="collapse" data-target="#data-5" aria-expanded="false" aria-controls="collapseExample" style="cursor:pointer;background-image : linear-gradient(to right, #5b0303, #a80808)">
            <h5 class="text-center text-white">{{strtoupper('Hazardous')}}</h5>
            <div class="collapse" id="data-5">
                <hr>
                <div class="hello text-white">
                Bersifat merugikan pada manusia ataupun kelompok hewan yang peka atau dapat menimbulkan kerusakan pada tumbuhan ataupun nilai estetika.
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>