<div class="card card-danger" id="index">
    <div class="card-header"><h4>Rate of Index</h4></div>
    <div class="card-body bg-secondary scrolling-wrapper no-gutter box">
        <?php $kecamatan = [0=>"Semarang Selatan",1=>"Semarang Tengah",2=>"Semarang Timur",3=>"Semarang Utara",4=>"Tembalang",5=>"Tugu",
                            6=>"Mijen",7=>"Ngaliyan",8=>"Pedurungan",9=>"Semarang Barat",
                            10=>"Banyumanik",11=>"Candisari",12=>"Gajahmungkur",13=>"Gayamsari",14=>"Genuk",15=>"Gunungpati"] ;
                                
        ?>
        @for($e=0;$e< count($kecamatan);$e++)
        <?php 
            $data = \App\CircleMaps::where('kecamatan',$kecamatan[$e])->get();
            $gas = 0;
            for ($a=0; $a < count($data); $a++) { 
               $gas += ($data[$a]->gasA +$data[$a]->gasB) /2;
            }
            $value=0;
            if($gas >0){
                $value= $gas/count($data);
            }
            if($value >=0 && $value < 51 ){
                $colorBG = 'linear-gradient(to right, #11c614, #00ff2e)' ;
            }
            if($value>50 && $value< 101){
                $colorBG = 'linear-gradient(to right, #f7e418, #e8e400)' ;
            }
            if($value>100 && $value< 200){
                $colorBG = 'linear-gradient(to right, #ff9400, #ffc300)' ;
            }
            if($value>200 && $value< 301){
                $colorBG = 'linear-gradient(to right, #ff0c0c, #fc2828)' ;
            }
            if($value>300){
                $colorBG = 'linear-gradient(to right, #5b0303, #a80808)' ;
            }
        ?>
        
        <div class="col-md-3 box-1">
            <div class="card">
            <div class="card-header">
                <h4>Kec. {{$kecamatan[$e]}}</h4>
                <div class="card-header-action">
                <a data-collapse="#kec-{{$e}}" class="btn btn-icon btn-info" href="#"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="collapse show" id="mycard-1">
                <div class="card-body" data-toggle="collapse" data-target=".kecamatan" aria-expanded="false" aria-controls="collapseExample"  style="cursor: pointr;background-image : {{$colorBG}}">
                <h5 class="text-center text-white text-white"> {{number_format($value,2)}}</h5>
                <div class="collapse kecamatan" id="kec-{{$e}}">
                    <hr>
                    <div class="hello text-white">
                        <p>
                        @foreach($data as $_data)
                            Lokasi  : {{$_data->lokasi}} <br>
                            API     : {{($_data->gasA+$_data->gasB)/2}} <hr>
                        @endforeach
                        </p>
                    </div>
                </div>
                </div> 
            </div>
            </div>
        </div>
        @endfor

    </div>
</div>