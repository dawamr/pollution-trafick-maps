<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'admin','middleware'=>['auth']], function () {
    Route::get('/', function(){
        return redirect('/admin/features');
    });
    Route::get('/features','AdminController@index');
    
    Route::post('/features/add','AdminController@add');
    Route::post('/features/{id}/edit','AdminController@edit');
    Route::post('/{id}','AdminController@destroy');

});
// Route::group(['prefix'=>'features'], function () {
//     Route::get('/','ClientnController@index');
//     Route::get('/features','ClientnController@index');

// });
Route::get('/logout','HomeController@logout');

Route::get('/maps', 'ClientnController@index');
Route::get('/', 'ClientnController@index');
Route::get('/machine/{id}/{gasA}/{gasB}','ClientnController@MachinePost');
Route::get('/machine/{id}','ClientnController@MachineGet');
Route::get('/machine','ClientnController@MachineGetAll');
Route::get('/api/search', 'ClientnController@apiSearch');
Route::get('/search', 'ClientnController@search');
Route::get('/info/{location}', 'ClientnController@infoLoc');
Route::get('/shell/php-artisan-migrate-fresh', function() {
     Artisan::call('migrate:fresh');
    return redirect()->back();
});
Route::get('/shell/php-artisan-db-seed', function() {
     Artisan::call('db:seed');
    return redirect()->back();
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
